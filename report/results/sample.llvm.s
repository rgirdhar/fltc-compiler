	.section .mdebug.abi32
	.previous
	.file	"../report/results/sample.llvm"
	.text
	.align	2
	.type	echo,@function
	.ent	echo                    # @echo
echo:
	.frame	$sp,32,$ra
	.mask 	0x80000000,-4
	.fmask	0x00000000,0
# BB#0:                                 # %entry
	.set	noreorder
	.cpload	$25
	.set	nomacro
	addiu	$sp, $sp, -32
	sw	$ra, 28($sp)
	.cprestore	16
	addu	$6, $zero, $5
	addu	$5, $zero, $4
	lw	$2, %got($.str)($gp)
	nop
	addiu	$4, $2, %lo($.str)
	lw	$25, %call16(printf)($gp)
	nop
	jalr	$25
	nop
	lw	$2, 16($sp)
	nop
	addu	$gp, $zero, $2
	lw	$ra, 28($sp)
	nop
	addiu	$sp, $sp, 32
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	echo
$tmp0:
	.size	echo, ($tmp0)-echo

	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
	.frame	$sp,72,$ra
	.mask 	0x801C0000,-8
	.fmask	0x00000000,0
# BB#0:                                 # %entry
	.set	noreorder
	.cpload	$25
	.set	nomacro
	addiu	$sp, $sp, -72
	sw	$ra, 64($sp)
	.cprestore	16
	sw	$16, 48($sp)
	sw	$17, 52($sp)
	sw	$18, 56($sp)
	addiu	$2, $sp, 40
	addiu	$3, $zero, 10
	ori	$16, $2, 4
	sw	$zero, 40($sp)
	sw	$3, 0($16)
	lw	$17, 16($sp)
	nop
	addiu	$18, $zero, 0
$BB1_1:                                 # %loop
                                        # =>This Inner Loop Header: Depth=1
	lw	$2, 0($16)
	nop
	addiu	$3, $zero, 2
	addiu	$5, $2, 2
	sltu	$2, $5, $3
	lw	$3, 40($sp)
	nop
	addu	$2, $2, $18
	addu	$4, $3, $2
	sw	$4, 40($sp)
	sw	$5, 0($16)
	lw	$25, %call16(echo)($gp)
	nop
	jalr	$25
	nop
	addu	$gp, $zero, $17
	lw	$2, 40($sp)
	nop
	xor	$3, $2, $18
	sltu	$3, $3, 1
	beq	$3, $zero, $BB1_3
	nop
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	lw	$2, 0($16)
	nop
	sltiu	$2, $2, 100
	j	$BB1_4
	nop
$BB1_3:                                 # %loop
                                        #   in Loop: Header=BB1_1 Depth=1
	slti	$2, $2, 0
$BB1_4:                                 # %loop
                                        #   in Loop: Header=BB1_1 Depth=1
	bne	$2, $zero, $BB1_1
	nop
# BB#5:                                 # %else
	lw	$18, 56($sp)
	nop
	lw	$17, 52($sp)
	nop
	lw	$16, 48($sp)
	nop
	lw	$ra, 64($sp)
	nop
	addiu	$sp, $sp, 72
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	main
$tmp1:
	.size	main, ($tmp1)-main

	.type	$.str,@object           # @.str
	.section	.rodata,"a",@progbits
	.align	2
$.str:
	.asciz	 "%d\n"
	.size	$.str, 4

