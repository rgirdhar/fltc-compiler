#include "node.h"
#include "codegen.h"
#include "parser.hpp"

using namespace std;

/* Compile the AST into a module */
void CodeGenContext::generateCode(NBlock& root)
{
        std::cout << "Generating code...\n";
        
        /* Create the top level interpreter function to call as entry */
        vector<Type*> argTypes;
        FunctionType *ftype = FunctionType::get(Type::getVoidTy(getGlobalContext()), makeArrayRef(argTypes), false);
        mainFunction = Function::Create(ftype, GlobalValue::InternalLinkage, "main", module);
        BasicBlock *bblock = BasicBlock::Create(getGlobalContext(), "entry", mainFunction, 0);
        
        /* Push a new variable/block context */
        pushBlock(bblock);
        root.codeGen(*this); /* emit bytecode for the toplevel block */
        ReturnInst::Create(getGlobalContext(), currentBlock());
        popBlock();
        
        /* Print the bytecode in a human-readable format
         to see if our program compiled properly
         */
        std::cout << "Code is generated.\n";
        PassManager pm;
        pm.add(createPrintModulePass(&outs()));
        pm.run(*module);
}

/* Executes the AST by running the main function */
GenericValue CodeGenContext::runCode() {
        std::cout << "Running code...\n";
        ExecutionEngine *ee = EngineBuilder(module).create();
        vector<GenericValue> noargs;
        GenericValue v = ee->runFunction(mainFunction, noargs);
        std::cout << "Code was run.\n";
        return v;
}

/* Returns an LLVM type based on the identifier */
static Type *typeOf(const NIdentifier& type)
{
        if (type.name.compare("int") == 0) {
                return Type::getInt64Ty(getGlobalContext());
        } else if (type.name.compare("char") == 0) {
            return Type::getInt8Ty(getGlobalContext());
        } else if (type.name.compare("boolean") == 0) {
            return Type::getInt1Ty(getGlobalContext());
        }
        else if (type.name.compare("double") == 0) {
                return Type::getDoubleTy(getGlobalContext());
        }
        return Type::getVoidTy(getGlobalContext());
}

/* -- Code Generation -- */
Value* NIdentifier::codeGen(CodeGenContext& context)
{
        std::cout << "Creating identifier reference: " << name << endl;
        if (context.locals().find(name) == context.locals().end()) {
                std::cerr << "undeclared variable " << name << endl;
                return NULL;
        }
        return new LoadInst(context.locals()[name], "", false, context.currentBlock());
}

Value* NBlock::codeGen(CodeGenContext& context)
{
        StatementList::const_iterator it;
        Value *last = NULL;

        for (int i = 0; i < vars.size(); i++) {
            vars[i]->codeGen(context);
        }


        for (it = statements.begin(); it != statements.end(); it++) {
                std::cout << "Generating code for " << typeid(**it).name() << endl;
                last = (**it).codeGen(context);
        }

        std::cout << "Creating block" << endl;
        return last;
}


Value* NVariableDeclaration::codeGen(CodeGenContext& context)
{
        std::cout << "Creating variable declaration " << type.name << " " << id.name << endl;
        AllocaInst *alloc = new AllocaInst(typeOf(type), id.name.c_str(), context.currentBlock());
        context.locals()[id.name] = alloc;
        return alloc;
}

Value* NAssignment::codeGen(CodeGenContext& context)
{
        std::cout << "Creating assignment for " << lhs.name << endl;
        if (context.locals().find(lhs.name) == context.locals().end()) {
                std::cerr << "undeclared variable " << lhs.name << endl;
                return NULL;
        }
        return new StoreInst(rhs.codeGen(context), context.locals()[lhs.name], false, context.currentBlock());
}


Value* NInteger::codeGen(CodeGenContext& context)
{
        std::cout << "Creating integer: " << value << endl;
        return ConstantInt::get(Type::getInt64Ty(getGlobalContext()), value, true);
}

Value* NBinaryOperator::codeGen(CodeGenContext& context)
{
        std::cout << "Creating binary operation " << op << endl;
        Instruction::BinaryOps instr;
        Instruction::OtherOps otherInstr;
        int done = 0;
        switch (op) {
                case TPLUS:         instr = Instruction::Add; done = 1; break;
                case TMINUS:        instr = Instruction::Sub;  done = 1; break;
                case TMUL:          instr = Instruction::Mul;  done = 1; break;
                case TDIV:          instr = Instruction::SDiv;  done = 1; break;;
                                
                /* TODO comparison */
        }
        if (done == 1) {
            return BinaryOperator::Create(instr, lhs.codeGen(context),
                rhs.codeGen(context), "", context.currentBlock());
        }
        
        unsigned short type = -1;
        switch (op) {
            case TCLT: otherInstr = Instruction::ICmp; 
                       type = ICmpInst::ICMP_SLT;
                       done = 2; break;
        }
        if (done == 2) {
            return CmpInst::Create(otherInstr,  type, lhs.codeGen(context), 
                    rhs.codeGen(context), "", context.currentBlock());
        }

        return NULL;
}

Value* NMethodCall::codeGen(CodeGenContext& context)
{
        //Function *function = context.module->getFunction(id.name.c_str());
        Function *function = context.module->getFunction("echo");
        if (function == NULL) {
                std::cerr << "no such function " << id.name << endl;
        }
        std::vector<Value*> args;
        ExpressionList::const_iterator it;
        for (it = arguments.begin(); it != arguments.end(); it++) {
                args.push_back((**it).codeGen(context));
        }
        CallInst *call = CallInst::Create(function, makeArrayRef(args), "", context.currentBlock());
        std::cout << "Creating method call: " << id.name << endl;
        return call;
}

Value* NExpressionStatement::codeGen(CodeGenContext& context)
{
        std::cout << "Generating code for " << typeid(expression).name() << endl;
        return expression.codeGen(context);
}

Value* NLabelStatement::codeGen(CodeGenContext& context) {
    /**
     * TODO : Handle the case when label occurs after the Goto
     */
    std::cout << "Gen Code for Label def: " << id.name << endl;

    BasicBlock* label_label1 = BasicBlock::Create(getGlobalContext(), id.name.c_str(), context.mainFunction ,0);    
//    context.pushBlock(label_label1);
    BranchInst::Create(label_label1, context.currentBlock());
    context.blocks.top()->block = label_label1;
    context.localLabels()[id.name] = label_label1;
    return label_label1;
}

Value* NGotoStatement::codeGen(CodeGenContext& context) {
    // TODO : Handle label generated later
    std::cout << "Gen Code for Goto : " << id.name << endl;
    std::string name = id.name;
    BasicBlock *label = context.localLabels()[name];
    if (isConditional) {
        Value* cond = expr.codeGen(context);   
        BasicBlock* if_else = BasicBlock::Create(getGlobalContext(), "else", context.mainFunction ,0);    
        BasicBlock* if_then = BasicBlock::Create(getGlobalContext(), "then", context.mainFunction ,0);    

        BranchInst::Create(if_then, if_else, cond, context.currentBlock());
        context.blocks.top()->block = if_then;

        BranchInst::Create(label, context.currentBlock());

        context.blocks.top()->block = if_else;
        context.localLabels()[id.name] = if_else;
    } else {
        BranchInst::Create(label, context.currentBlock());
    }
    return label;
}
