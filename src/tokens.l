%{
#include <string>
#include "node.h"
#include "parser.hpp"
#define SAVE_TOKEN yylval.string = new std::string(yytext, yyleng)
#define TOKEN(t) (yylval.token = t)
extern "C" int yywrap() { }
%}

%%

[ \t]					;
\n                      yylineno = yylineno + 1;
"class"                 return TOKEN(TCLASS);
"main"                  return TOKEN(TMAIN);
"int"                   return TOKEN(TINT);
"boolean"               return TOKEN(TBOOLEAN);
"char"                  return TOKEN(TCHAR);
"if"                    return TOKEN(TIF);
"then"                  return TOKEN(TTHEN);
"goto"                  return TOKEN(TGOTO);
"print"                 return TOKEN(TPRINT);
"read"                  return TOKEN(TREAD);
"true"                  return TOKEN(TTRUE);
"false"                 return TOKEN(TFALSE);
L?\"(\\.|[^\\"])*\"     return TOKEN(TSTRING_LITERAL);
'[^\']'                 return TOKEN(TCHAR_LITERAL);
[a-zA-Z_][a-zA-Z0-9_]* 	SAVE_TOKEN; return TIDENTIFIER;
[0-9]+					SAVE_TOKEN; return TINTEGER;
"="						return TOKEN(TEQUAL);
"=="					return TOKEN(TCEQ);
"!="					return TOKEN(TCNE);
"<"						return TOKEN(TCLT);
"<="					return TOKEN(TCLE);
">"						return TOKEN(TCGT);
">="					return TOKEN(TCGE);
"&&"                    return TOKEN(TAND);
"||"                    return TOKEN(TOR);
"("						return TOKEN(TLPAREN);
")"						return TOKEN(TRPAREN);
"["                     return TOKEN(TLSQBRACE);
"]"                     return TOKEN(TRSQBRACE);
"{"						return TOKEN(TLBRACE);
"}"						return TOKEN(TRBRACE);
"."						return TOKEN(TDOT);
","						return TOKEN(TCOMMA);
"+"						return TOKEN(TPLUS);
"-"						return TOKEN(TMINUS);
"*"						return TOKEN(TMUL);
"/"						return TOKEN(TDIV);
":"                     return TOKEN(TCOLON);
";"                     return TOKEN(TSEMICOLON);
"!"                     return TOKEN(TEXCLAIM);
.						printf("Unknown token : >%s<!\n", yytext); yyterminate();

%%
