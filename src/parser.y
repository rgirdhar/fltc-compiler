%{
    #include "node.h"
    #include <string>
    #include <iostream>
    extern int yylex();
    extern void yyerror(const char*);
    NBlock* programBlock;
%}

/* Represents the many different ways we can access our data */
%union {
    NBlock *block;
    NStatement *stmt;
    NIdentifier *ident;
    StatementList *stmtlist;
    VariableList *varlist;
    ExpressionList *exprvec;
	std::string *string;
    NExpression *expr;
	int token;
}

/* Define our terminal symbols (tokens). This should
   match our tokens.l lex file. We also define the node type
   they represent.
 */
%token <string> TIDENTIFIER TINTEGER TSTRING_LITERAL TCHAR_LITERAL
%token <token> TTRUE TFALSE
%token <token> TCLASS TMAIN TINT TCHAR TBOOLEAN
%token <token> TIF TTHEN TGOTO TREAD TPRINT
%token <token> TCEQ TCNE TCLT TCLE TCGT TCGE TEQUAL
%token <token> TLPAREN TRPAREN TLBRACE TRBRACE TCOMMA TDOT TCOLON TLSQBRACE TRSQBRACE TSEMICOLON
%token <token> TPLUS TMINUS TMUL TDIV TEXCLAIM TAND TOR

/* 
   Define the type of node our nonterminal symbols represent.
   The types refer to the %union declaration above. Ex: when
   we call an ident (defined by union type ident) we are really
   calling an (NIdentifier*). It makes the compiler happy.
 */

%type <ident> ident
%type <block> program
%type <stmtlist> stmts stmt labelled_stmt
%type <varlist> field_decls field_decl
%type <ident> type location label_ident
%type <ident> var vars 
%type <expr> expr method_call 
%type <string> literal
%type <exprvec> arguments

/* Operator precedence for mathematical operators */
%right TEQUAL
%left TOR
%left TAND
%left TCEQ TCNE
%left TCLT TCGT TCLE TCGE
%left TPLUS TMINUS
%left TMUL TDIV
%right TEXCLAIM

%start program

%%

program : TCLASS TMAIN TLBRACE field_decls stmts TRBRACE 
        { 
            programBlock = new NBlock(); 
            programBlock->vars = *$4; 
            programBlock->statements = *$5; 
        }
		;

field_decls : /* empty */ { $$ = new VariableList(); } 
            | field_decls field_decl
            {
                $2->insert($2->end(), $1->begin(), $1->end()); $$ = $2;
            }
            ;

field_decl : type vars TSEMICOLON 
           { $$ = new VariableList(); 
             $$->push_back(new NVariableDeclaration(*$1, *$2)); 
           }
           ;

type : TINT { $$ = new NIdentifier(std::string("int")); }
     | TBOOLEAN { $$ = new NIdentifier(std::string("boolean")); }
     | TCHAR { $$ = new NIdentifier(std::string("char")); }
     ;
		
stmts : /* empty */ { $$ = new StatementList(); }
	  | stmt stmts 
      { 
        $1->insert($1->end(), $2->begin(), $2->end()); $$ = $1; 
        delete $2;
      }
	  ;

stmt : labelled_stmt
     | location TEQUAL expr TSEMICOLON
     { $$ = new StatementList(); $$->push_back(new NAssignment(*$1, *$3)); }
     | TIF expr TTHEN TGOTO label_ident TSEMICOLON
     {
        $$ = new StatementList();
        $$->push_back(new NGotoStatement(*$5, *$2));
     }
     | TGOTO label_ident TSEMICOLON
     {
        $$ = new StatementList();
        $$->push_back(new NGotoStatement(*$2));
     }
     | method_call 
     { 
        $$ = new StatementList(); 
        $$->push_back(new NExpressionStatement(*$1)); 
     }
     ;

labelled_stmt : label_ident TCOLON stmt
              {
                $$ = $3;
                $$->insert($$->begin(), new NLabelStatement(*$1));
              }
              ;

label_ident : ident
            ;

location : ident
         | ident TLSQBRACE expr TRSQBRACE
         ;

expr : literal { $$ = new NInteger(atol($1->c_str())); delete $1; }
     | location
     | expr TPLUS expr { $$ = new NBinaryOperator(*$1, $2, *$3); }
     | expr TMINUS expr { $$ = new NBinaryOperator(*$1, $2, *$3); }
     | expr TMUL expr { $$ = new NBinaryOperator(*$1, $2, *$3); }
     | expr TDIV expr { $$ = new NBinaryOperator(*$1, $2, *$3); }
     | expr TCLT expr { $$ = new NBinaryOperator(*$1, $2, *$3); }
     | expr TCLE expr
     | expr TCGT expr
     | expr TCGE expr
     | expr TCEQ expr
     | expr TCNE expr
     | expr TAND expr
     | expr TOR expr
     | TMINUS expr
     | TEXCLAIM expr
     | TLPAREN expr TRPAREN
     ;

method_call : TPRINT TLPAREN arguments TRPAREN TSEMICOLON 
            { 
            $$ = new NMethodCall(*(new NIdentifier(*(new std::string("echo")))), *$3); 
            }
            | TREAD TLPAREN location TRPAREN TSEMICOLON
            ;

arguments   : arguments TCOMMA expr { $$ = $1; $$->push_back($3); }
            | expr { $$ = new ExpressionList(); $$->push_back($1); }
            ;

literal : int_literal 
        | string_literal
        | char_literal
        | bool_literal
        ;

int_literal : TINTEGER
            ;

bool_literal : TTRUE
             | TFALSE
             ;

string_literal : TSTRING_LITERAL
               ;

char_literal : TCHAR_LITERAL
             ;

vars : var 
     | vars TCOMMA var 
     { $$ = $3; /* TODO: handle multiple!!*/ }
     ;

var : ident
    | ident TLSQBRACE int_literal TRSQBRACE 

ident : TIDENTIFIER { $$ = new NIdentifier(*$1); }
	  ;

%%

