#include <iostream>
#include <cstdio>
#include "codegen.h"
#include "node.h"
#include "parser.hpp"

using namespace std;

extern int yyparse();
extern int yylineno;
extern NBlock* programBlock;

void createCoreFunctions(CodeGenContext& context);

void yyerror(const char* s) {
    printf("Error %s in line no %d\n", s, yylineno);
}

int main(int argc, char **argv)
{
	if (!yyparse()) {
        printf("correct\n");
    } else {
        printf("not correct\n");
    }
    
    InitializeNativeTarget();
    CodeGenContext context;
    createCoreFunctions(context);
    context.generateCode(*programBlock);
	
	return 0;
}

