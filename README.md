FLTC Compiler
=============
Rohit Girdhar
201001047

Acknowledgement
---------------
Thanks a lot [lsegal](https://github.com/lsegal "github") 
for the awesome [tutorial](http://gnuu.org/2009/09/18/writing-your-own-toy-compiler/ "writing your own toy compiler")!
I used some of the above code for parts of this project.

To compile and run
------------------
```bash
$ make
$ ./parser < [source input file, example.txt]
```

Lexical Analyzer (using flex)
-----------------------------
src/tokens.l file contains the lexical analyzer definitions
for the FLTC compiler.
I define tokens for each of the keywords, general identifier syntax,
relational operators

Syntax Analyzer (using bison)
-----------------------------
src/parser.y file contains the syntax definitions of the FLTC
as CFG. The stack can have string or integer (though not important 
at this stage). 
I first define the type of terminal symbols used in the lex file.
I define the precedence of operators in order to avoid shift-reduce
errors in definition of expressions. The parser generator takes into account
the precedence and generates the DFA.

I then define all the rules of the grammar using CFG notation.
`A*` type production are handled by making 2 productions:
A -> <empty> | a A;
`A+` is made into
A -> a A

label is defined same as identifier

I'd to put all the arithmetic operator definitions into the expr
production (unlike as given in question) because otherwise it gave 
a shift-reduce error at the following state:

state 83

expr -> expr . binop expr
expr -> expr binop expr .

In the above, parser got confused whether to reduce the 
`expr binop expr` or wait for the further `binop`. 
Adding all the operators together resolves this issue.

Code Generation
---------------
Details in Report
